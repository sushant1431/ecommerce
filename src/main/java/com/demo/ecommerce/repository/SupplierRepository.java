package com.demo.ecommerce.repository;

import com.demo.ecommerce.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {

}
