package com.demo.ecommerce.controller;

import com.demo.ecommerce.model.Product;
import com.demo.ecommerce.model.Supplier;
import com.demo.ecommerce.repository.SupplierRepository;
import com.demo.ecommerce.service.ProductService;
import com.demo.ecommerce.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService productService;
    private SupplierService supplierService;

    public ProductController(ProductService productService, SupplierService supplierService) {
        this.productService = productService;
        this.supplierService = supplierService;
    }

    @GetMapping("/products")
    public @NotNull List<Product> getProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/group/products")
    public @NotNull Map<String, List<Product>> getProductsGroupBy(@RequestParam(value = "by", defaultValue = "color") String groupBy) {
        return productService.getProductsGroupBy(groupBy);
    }

    @GetMapping(value = "/product/{sku}")
    public @NotNull Product getProduct(@PathVariable("sku") int sku) {
        return productService.getProduct(sku);
    }

    @GetMapping("/suppliers")
    public List<Supplier> getSuppliers() {
        return supplierService.getAllSupplier();
    }

    @PostMapping("/product/{supplierId}")
    public Product addProduct(@RequestBody Product product, @PathVariable long supplierId){
        return productService.addProduct(product, supplierId);
    }

    @DeleteMapping("/product/{productId}")
    public void addProduct(@PathVariable long supplierId){
        productService.deleteProduct(supplierId);
    }


}