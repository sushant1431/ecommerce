package com.demo.ecommerce.service;

import com.demo.ecommerce.exception.ResourceNotFoundException;
import com.demo.ecommerce.model.Product;
import com.demo.ecommerce.model.Supplier;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.repository.SupplierRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private SupplierService supplierService;

    public ProductServiceImpl(ProductRepository productRepository, SupplierService supplierService) {
        this.productRepository = productRepository;
        this.supplierService = supplierService;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProduct(long sku) {
        return productRepository
                .findById(sku)
                .orElseThrow(() -> new ResourceNotFoundException("Product not found"));
    }

    @Override
    public Product addProduct(Product product, long supplierId) {
        Supplier supplier = supplierService.getSupplier(supplierId);
        product.setSupplier(supplier);
        return save(product);
    }

    @Override
    public void deleteProduct(long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public Map<String, List<Product>> getProductsGroupBy(String groupBy) {
        Map<String, List<Product>> groupByObj;
        Stream<Product> productStream = getAllProducts().stream();
        switch (groupBy) {
            case "color":
                groupByObj = productStream.collect(Collectors.groupingBy(Product::getColor));
                break;
            case "size":
                groupByObj = productStream.collect(Collectors.groupingBy(Product::getSize));
                break;
            case "price":
                groupByObj = productStream.collect(Collectors.groupingBy(Product::getStringPrice));
                break;
            default:
                groupByObj = productStream.collect(Collectors.groupingBy(Product::getBrandName));

        }
        return groupByObj;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

}