package com.demo.ecommerce.service;

import com.demo.ecommerce.model.Product;
import com.demo.ecommerce.model.Supplier;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface SupplierService {

    @NotNull List<Supplier> getAllSupplier();

    Supplier save(Supplier product);

    Supplier getSupplier(long supplierId);
}
