package com.demo.ecommerce.service;


import com.demo.ecommerce.model.Product;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Validated
public interface ProductService {

    @NotNull List<Product> getAllProducts();

    Product getProduct(@Min(value = 1L, message = "Invalid product ID.") long id);

    Map<String, List<Product>> getProductsGroupBy(String groupBy);

    Product save(Product product);

    Product addProduct(Product product, long supplierId);

    void deleteProduct(long supplierId);
}