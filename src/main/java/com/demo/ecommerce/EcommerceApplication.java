package com.demo.ecommerce;

import com.demo.ecommerce.model.Product;
import com.demo.ecommerce.model.Supplier;
import com.demo.ecommerce.service.ProductService;
import com.demo.ecommerce.service.SupplierService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EcommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ProductService productService, SupplierService supplierService) {
		return args -> {
			Supplier supplierA = new Supplier(1L,"Supplier A");
			Supplier supplierB = new Supplier(2L,"Supplier B");
			Supplier supplierC = new Supplier(3L,"Supplier C");
			Supplier supplierD = new Supplier(4L,"Supplier D");
			Supplier supplierE = new Supplier(5L,"Supplier E");

			supplierService.save(supplierA);
			supplierService.save(supplierB);
			supplierService.save(supplierC);
			supplierService.save(supplierD);
			supplierService.save(supplierE);

			productService.save(new Product(1L, "Shirt", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(2L, "Coat", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(3L, "Pant", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(4L, "Trouser", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(5L, "Trouser", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(6L, "Jeans", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(7L, "Tie", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));
			productService.save(new Product(8L, "Handkerchief", 1000.00, "http://placehold.it/200x100", "Raymond", "Blue", "XL",supplierA));


			productService.save(new Product(11L, "Shirt", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(12L, "Coat", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(13L, "Pant", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(14L, "Trouser", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(15L, "Trouser", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(16L, "Jeans", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(17L, "Tie", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));
			productService.save(new Product(18L, "Handkerchief", 1000.00, "http://placehold.it/200x100", "USPA", "Blue", "XL",supplierB));


			productService.save(new Product(21L, "Shirt", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(22L, "Coat", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(23L, "Pant", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(24L, "Trouser", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(25L, "Trouser", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(26L, "Jeans", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(27L, "Tie", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));
			productService.save(new Product(28L, "Handkerchief", 1000.00, "http://placehold.it/200x100", "POLO", "Blue", "XL",supplierC));


			productService.save(new Product(31L, "Shirt", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(32L, "Coat", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(33L, "Pant", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(34L, "Trouser", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(35L, "Trouser", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(36L, "Jeans", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(37L, "Tie", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));
			productService.save(new Product(38L, "Handkerchief", 1000.00, "http://placehold.it/200x100", "Wrangler", "Blue", "XL",supplierD));


			productService.save(new Product(41L, "Shirt", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(42L, "Coat", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(43L, "Pant", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(44L, "Trouser", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(45L, "Trouser", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(46L, "Jeans", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(47L, "Tie", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
			productService.save(new Product(48L, "Handkerchief", 1000.00, "http://placehold.it/200x100", "Khadi", "Blue", "XL",supplierE));
		};
	}

}
