package com.demo.ecommerce;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
class EcommerceApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllProducts() throws Exception {
        mockMvc.perform(get("/api/products")).andDo(print());
    }

    @Test
    void getAllProductsGroupBy() throws Exception {
        mockMvc.perform(get("/api/group/products?by=color")).andDo(print());
    }

    void verifyProductDeleteTest() {
    }

    void verifyProductAddTest() {
    }

    void verifyProductUpdateTest() {
    }

}
